package ee.ut.math.tvt.salessystem.ui.tabs;

import java.awt.Component;

import javafx.scene.control.Tab;
import javafx.scene.layout.AnchorPane;

import javax.swing.JPanel;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "History" in the menu).
 */
public class HistoryTab extends AnchorPane {
    
    // TODO - implement!

    public HistoryTab() {
    	super();
    } 
    
    public Component draw() {
        JPanel panel = new JPanel();
        // TODO - Sales history table
        return panel;
    }
}
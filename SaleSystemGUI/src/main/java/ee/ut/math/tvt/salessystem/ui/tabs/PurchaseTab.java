package ee.ut.math.tvt.salessystem.ui.tabs;

import ee.ut.math.tvt.salessystem.domain.data.SoldItem;
import ee.ut.math.tvt.salessystem.domain.data.StockItem;
import ee.ut.math.tvt.salessystem.domain.exception.VerificationFailedException;
import ee.ut.math.tvt.salessystem.domain.controller.SalesDomainController;
import ee.ut.math.tvt.salessystem.ui.model.SalesSystemModel;

import java.io.IOException;
import java.util.NoSuchElementException;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;

import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.LogManager;

/**
 * Encapsulates everything that has to do with the purchase tab (the tab
 * labelled "Point-of-sale" in the menu).
 */
public class PurchaseTab extends AnchorPane {

  private static final Logger log = LogManager.getLogger(PurchaseTab.class);

  private final SalesDomainController domainController;

  @FXML private Button newPurchase;
  @FXML private Button submitPurchase;
  @FXML private Button cancelPurchase;
  
  @FXML private TextField barCodeField;
  @FXML private TextField quantityField;
  @FXML private TextField nameField;
  @FXML private TextField priceField;

  @FXML private Button addItemButton;
  
  @FXML private TableView purchaseTableView;


  private SalesSystemModel model;
  
//  public PurchaseTab(){
//	  super();
//	  domainController = SalesDomainControllerImpl.getInstance();
//	  model = new SalesSystemModel(domainController);
//  }


  public PurchaseTab(SalesDomainController controller,
      SalesSystemModel model)
  {
	super();
	this.domainController = controller;
    this.model = model;
    
    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("PurchaseTab.fxml"));
    fxmlLoader.setRoot(this);
    fxmlLoader.setController(this);

    try {
        fxmlLoader.load();
    } catch (IOException exception) {
        throw new RuntimeException(exception);
    }
    
    //Disable cancel and submit button
    cancelPurchase.setDisable(true);
    submitPurchase.setDisable(true);
    purchaseTableView.setItems(model.getCurrentPurchaseTableModel());
    disableProductField(true);
    
    this.barCodeField.focusedProperty().addListener(new ChangeListener<Boolean>() {
        @Override
        public void changed(ObservableValue<? extends Boolean> arg0, Boolean oldPropertyValue, Boolean newPropertyValue) {
            if (newPropertyValue) {
                System.out.println("Textfield on focus");
            }
            else {
            	fillDialogFields();
            }
        }
    });
  }


  /**
   * The purchase tab. Consists of the purchase menu, current purchase dialog and
   * shopping cart table.
   */

  
//  /** Event handler for the <code>new purchase</code> event. */
  @FXML protected void newPurchaseButtonClicked() {
    log.info("New sale process started");
    try {
      domainController.startNewPurchase();
      startNewSale();
    } catch (VerificationFailedException e1) {
      log.error(e1.getMessage());
    }
  }


  /**  Event handler for the <code>cancel purchase</code> event. */
  @FXML protected void cancelPurchaseButtonClicked() {
    log.info("Sale cancelled");
    try {
      domainController.cancelCurrentPurchase();
      endSale();
      model.getCurrentPurchaseTableModel().clear();
    } catch (VerificationFailedException e1) {
      log.error(e1.getMessage());
    }
  }


  /** Event handler for the <code>submit purchase</code> event. */
  @FXML protected void submitPurchaseButtonClicked() {
    log.info("Sale complete");
    try {
      log.debug("Contents of the current basket:\n" + model.getCurrentPurchaseTableModel());
      domainController.submitCurrentPurchase(
          model.getCurrentPurchaseTableModel().getTableRows()
      );
      endSale();
      model.getCurrentPurchaseTableModel().clear();
    } catch (VerificationFailedException e1) {
      log.error(e1.getMessage());
    }
  }



  /* === Helper methods that bring the whole purchase-tab to a certain state
   *     when called.
   */

  // switch UI to the state that allows to proceed with the purchase
  private void startNewSale() {
	resetProductField();
	disableProductField(false);
	cancelPurchase.setDisable(false);
	submitPurchase.setDisable(false);
    newPurchase.setDisable(true);
  }

  // switch UI to the state that allows to initiate new purchase
  private void endSale() {
	resetProductField();
    cancelPurchase.setDisable(true);
    submitPurchase.setDisable(true);
    newPurchase.setDisable(false);
    disableProductField(true);
  }
  
//Fill dialog with data from the "database".
  public void fillDialogFields() {
      StockItem stockItem = getStockItemByBarcode();

      if (stockItem != null) {
          nameField.setText(stockItem.getName());
          String priceString = String.valueOf(stockItem.getPrice());
          priceField.setText(priceString);
      } else {
    	  resetProductField();
      }
  }

  // Search the warehouse for a StockItem with the bar code entered
  // to the barCode textfield.
  private StockItem getStockItemByBarcode() {
      try {
          int code = Integer.parseInt(barCodeField.getText());
          return model.getWarehouseTableModel().getItemById(code);
      } catch (NumberFormatException ex) {
          return null;
      } catch (NoSuchElementException ex) {
          return null;
      }
  }

  /**
   * Add new item to the cart.
   */
  @FXML public void addItemEventHandler() {
      // add chosen item to the shopping cart.
      StockItem stockItem = getStockItemByBarcode();
      if (stockItem != null) {
          int quantity;
          try {
              quantity = Integer.parseInt(quantityField.getText());
          } catch (NumberFormatException ex) {
              quantity = 1;
          }
          model.getCurrentPurchaseTableModel()
              .addItem(new SoldItem(stockItem, quantity));
      }
  }

  /**
   * Sets whether or not the product component is enabled.
   */
  public void disableProductField(boolean disable) {
      this.addItemButton.setDisable(disable);
      this.barCodeField.setDisable(disable);
      this.quantityField.setDisable(disable);
      this.nameField.setDisable(disable);
      this.priceField.setDisable(disable);
  }

  /**
   * Reset dialog fields.
   */
  public void resetProductField() {
      barCodeField.setText("");
      quantityField.setText("1");
      nameField.setText("");
      priceField.setText("");
  }
}
